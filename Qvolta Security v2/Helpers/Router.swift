//
//  Router.swift
//  Qvolta Security v2
//
//  Created by Andrii Gusarov on 1/21/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import UIKit

struct Router {
    static func showCodeViewController() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CodeViewController") as! CodeController
        
        (UIApplication.shared.keyWindow?.rootViewController as! UINavigationController).setViewControllers([controller], animated: false)
    }
    
    static func showDashboardViewController() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        
        (UIApplication.shared.keyWindow?.rootViewController as! UINavigationController).setViewControllers([controller], animated: false)
    }
}
