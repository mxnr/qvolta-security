//
//  CodeHandler.swift
//  Qvolta Security v2
//
//  Created by Andrii Gusarov on 1/21/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import Foundation
import KeychainSwift

class CodeHandler {
    static let shared = CodeHandler()
    
    private let keychain = KeychainSwift()
    
    var code: String?  {
        get {
            return keychain.get("QvoltaSecuritySalt")
        }
        set {
            if let text = newValue, text.count > 0 {
                keychain.set(text, forKey: "QvoltaSecuritySalt")
            } else {
                keychain.delete("QvoltaSecuritySalt")
            }
        }
    }
    
    private init() {}
    
    
}
