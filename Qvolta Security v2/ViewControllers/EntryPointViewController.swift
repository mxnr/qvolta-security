//
//  EntryPointViewController.swift
//  Qvolta Security v2
//
//  Created by Andrii Gusarov on 1/21/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import UIKit

class EntryPointViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let duration = 0.9
        let delay = 0.0
        let options = UIViewKeyframeAnimationOptions.calculationModeCubic
        
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: options, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                self.logoImageView.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                self.logoImageView.alpha = 1.0
            })
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.6, animations: {
                self.logoImageView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                self.logoImageView.alpha = 0.0
            })
            
        }, completion: { finished in
            
            if let code = CodeHandler.shared.code, code.count > 0 {
                let transition: CATransition = CATransition()
                transition.duration = 0.3
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFade
                self.navigationController!.view.layer.add(transition, forKey: nil)
                
                Router.showCodeViewController()
            } else {
                Router.showDashboardViewController()
            }
        })
    
    }
}
