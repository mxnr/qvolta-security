//
//  BarCodeController.swift
//  Qvolta Security v2
//
//  Created by Evgeniy Sychov on 1/6/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import AVFoundation
import UIKit
import OTPKit

class BarCodeController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    let session = AVCaptureSession()
    var cameraDevice: AVCaptureDevice!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var captureLabel: UILabel!
    @IBOutlet weak var previewView: PreviewView!
    @IBOutlet weak var scannerIndicator: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == .authorized {
            showCamera()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {[weak self] (granted) in
                if !granted {
                    if Thread.isMainThread {
                        self?.showRestrictedAlert()
                    } else {
                        DispatchQueue.main.sync {
                            self?.showRestrictedAlert()
                        }
                    }
                } else {
                    self?.showCamera()
                }
            })
            
        case .restricted, .denied:
            showRestrictedAlert()
            break
        case .authorized:
            break
            
        }
        
    }
    
    func showRestrictedAlert() {
        let alert = UIAlertController(title: "Allow camera access",
                                      message: "To use this feature, allow Qvolta Security access to the camera in your device’s settings.",
                                      preferredStyle: .alert)
        
        let actionNotNow = UIAlertAction(title: "Not Now", style: .default) {
            (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(actionNotNow)
        
        let actionOk = UIAlertAction(title: "Setting", style: .default) {
            (action) in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        }
        alert.addAction(actionOk)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showCamera() {
        let serialQueue = DispatchQueue(label: "com.Qvolta.avfoundation")
        serialQueue.async { [weak self] in
            
            self?.session.startRunning()
            self?.cameraDevice = AVCaptureDevice.default(for: .video)
            if let cameraDevice = self?.cameraDevice, let possibleCameraInput = try? AVCaptureDeviceInput.init(device: cameraDevice) {
                
                if (self?.session.canAddInput(possibleCameraInput))! {
                    self?.session.addInput(possibleCameraInput)
                }
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            if (self?.session.canAddOutput(metadataOutput))! {
                self?.session.addOutput(metadataOutput)
            }
            
            if metadataOutput.availableMetadataObjectTypes
                .contains(AVMetadataObject.ObjectType.qr) {
                metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            }
            
            DispatchQueue.main.async {
                self?.previewLayer = AVCaptureVideoPreviewLayer.init(session: (self?.session)!)
                self?.previewLayer.frame = (self?.previewView.bounds)!
                self?.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self?.previewView.layer.insertSublayer((self?.previewLayer)!,
                                                       below:(self?.captureLabel.layer)!)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.goBackHome()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count > 0 {
            for metadataObj in metadataObjects {
                let metadataObj = metadataObj as! AVMetadataMachineReadableCodeObject
                if metadataObj.type == AVMetadataObject.ObjectType.qr {
                    found(code: metadataObj.stringValue!)
                }
            }
        }
        
    }
    
    var inProgress = false
    
    
    func captureOutput(_ output: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        guard inProgress else { return }
        
        
        inProgress = true
        for metadataObject in metadataObjects {
            
            guard let metadataObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            
            if  metadataObject.type == AVMetadataObject.ObjectType.qr {
                found(code: metadataObject.stringValue!)
                inProgress = false
                break
            }
            
        }
    }
    
    deinit {
        session.stopRunning()
        session.inputs.forEach { input in
            session.removeInput(input)
        }
        session.outputs.forEach { output in
            session.removeOutput(output)
        }
    }
    
    func showAlertIncorrectCode() {
        let alert = UIAlertController(title: NSLocalizedString("incorrect-code-title", comment: ""),
                                      message: NSLocalizedString("incorrect-code", comment: ""),
                                      preferredStyle: .alert)
        
        let actionOk = UIAlertAction(title: "OK", style: .default)
        alert.addAction(actionOk)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func found(code: String) {
        guard code.count > 0 else {
            showAlertIncorrectCode()
            
            return
        }
        
        guard let _ = try? Base32.decode(code) else {
            showAlertIncorrectCode()
            
            return
        }
        
        CodeHandler.shared.code = code
        
        Router.showCodeViewController()
        dismiss(animated: true, completion: nil)
    }
    
    
    func goBackHome() -> Void {
        dismiss(animated: true, completion: nil)
    }
}
