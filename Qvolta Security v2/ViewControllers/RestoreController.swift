//
//  RestoreController.swift
//  Qvolta Security v2
//
//  Created by Evgeniy Sychov on 1/6/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import UIKit
import OTPKit

class RestoreController: UIViewController {
    
    @IBOutlet weak var codeTextField: UITextField!
    
    @IBOutlet weak var requestButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeTextField.layer.borderWidth = 2.0
        requestButton.isEnabled = false
        requestButton.backgroundColor = UIColor(red: 29.0 / 255.0, green: 43.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 5))
        codeTextField.leftViewMode = .always
        codeTextField.leftView = paddingView
        
        setTextfieldClear()
    }
    
    func setTextfieldClear() {
        codeTextField.backgroundColor = .clear
        
        let color = UIColor(red: 60.0 / 255.0, green: 68.0 / 255.0, blue: 128.0 / 255.0, alpha: 0.5)
        
        codeTextField.layer.borderColor = color.cgColor
        codeTextField.attributedPlaceholder = NSAttributedString(string: codeTextField.placeholder!,
                                                                 attributes: [NSAttributedStringKey.foregroundColor: color])
    }
    
    func setTextfieldActive() {
        codeTextField.layer.borderColor = UIColor(red: 2.0 / 255.0, green: 198.0 / 255.0, blue: 192.0 / 255.0, alpha: 1.0).cgColor
        codeTextField.backgroundColor = UIColor.init(white: 1.0, alpha: 0.1)
    }
    
    func setTextfieldError() {
        codeTextField.layer.borderColor = UIColor(red: 229.0 / 255.0, green: 71.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0).cgColor
        codeTextField.backgroundColor = UIColor.init(white: 1.0, alpha: 0.1)
    }
    
    @IBAction func didBackgroundTap(_ sender: Any) {
        codeTextField.endEditing(true)
    }
    
    @IBAction func buttonRequestTouched(_ sender: Any) {
        
        codeTextField.endEditing(true)
        
        checkCode()
    }
    
    @IBAction func buttonCloseTouched(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func checkCode() {
        guard let code = codeTextField.text, code.count > 0 else {
            setTextfieldError()
            
            return
        }
        
        guard let _ = try? Base32.decode(code) else {
            setTextfieldError()
            
            return
        }
        
        CodeHandler.shared.code = code
        
        Router.showCodeViewController()
        dismiss(animated: true, completion: nil)
    }
}

extension RestoreController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setTextfieldActive()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            let newText = text.replacingCharacters(in: range, with: string)
            
            requestButton.isEnabled = (newText.count > 0) ? true : false
        }
        
        setTextfieldActive()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        
        checkCode()
        
        return true
    }
}
