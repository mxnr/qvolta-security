//
//  CodeController.swift
//  Qvolta Security v2
//
//  Created by Evgeniy Sychov on 1/6/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import Foundation
import UIKit
import OTPKit

class CodeController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var progressView: CircleProgressView!
    @IBOutlet weak var passwordLabel: UILabel!
    
    var passwordUpdateTimer: Timer!
    
    // MARK: - View methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(updateCircle),
                         name: NSNotification.Name.UIApplicationWillEnterForeground,
                         object: nil)
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(pauseCircle),
                         name: NSNotification.Name.UIApplicationWillResignActive,
                         object: nil)
        
        updateCircle()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        pauseCircle()
    }
    
    @objc func updateCircle() {
        let date = Date()
        let second: Int = Calendar.current.component(.second, from: date)
        
        startNewCycle(seconds: second)
        
        let fireDate = date.addingTimeInterval(TimeInterval(60 - second))
        
        passwordUpdateTimer = Timer.init(fire: fireDate,
                                         interval: 60,
                                         repeats: true) {[weak self] (timer) in
                                            self?.startNewCycle()
        }
        
        RunLoop.current.add(passwordUpdateTimer, forMode: .commonModes)
        
        progressView.layoutSubviews()
    }
    
    @objc func pauseCircle() {
        progressView.stop()
        passwordUpdateTimer?.invalidate()
    }
    
    // MARK: - IBActions
    
    @IBAction func didProgressViewTap(_ sender: Any) {
        UIPasteboard.general.string = passwordLabel.text
        
        descriptionLabel.text = NSLocalizedString("title-success-copied", comment: "")
    }
    
    // MARK: Cycle methods
    
    func startNewCycle(seconds: Int = 0) {
        descriptionLabel.text = NSLocalizedString("title-description-copy", comment: "")
        
        progressView.animate(from: Float(seconds) / 60.0)
        
        passwordLabel.text = drawOtpCode()
    }
    
    func drawOtpCode() -> String {
        // Shared unique secret (usually represented by a Base 32 string)
        let base32String = CodeHandler.shared.code!
        let secret = try! Base32.decode(base32String)
        let passwordGenerator = TOTPGenerator(key: secret, period: 60, digits: 6, hashFunction: .sha1)
        
        return try! passwordGenerator.password(for: Date()) // Password for current time
    }
}
