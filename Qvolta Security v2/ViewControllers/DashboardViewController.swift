//
//  ViewController.swift
//  Qvolta Security v2
//
//  Created by Evgeniy Sychov on 1/5/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var welcomeImageView: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var scanCodeButton: UIButton!
    
    @IBOutlet weak var lostYourAccessLabel: UIButton!
    
    @IBOutlet weak var restoreButton: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    private var elementsArray: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let code = CodeHandler.shared.code, code.count > 0 {
            closeButton.isHidden = false
        } else {
            closeButton.isHidden = true
        }
        
        elementsArray = [logoImageView, welcomeImageView, descriptionLabel, scanCodeButton, lostYourAccessLabel, restoreButton]
        
        elementsArray.forEach {
            $0.alpha = 0.0
            $0.transform = CGAffineTransform(translationX: 0, y: 30)
        }
        
        var enumerator = 0.0
        
        elementsArray.forEach { (view) in
            UIView.animate(withDuration: 0.5, delay: 0.2 + enumerator,
                           options: UIViewAnimationOptions.curveEaseOut,
                           animations: {
                            view.alpha = 1.0
                            view.transform = CGAffineTransform.identity
            }, completion: nil)
            enumerator = enumerator + 0.1
        }
    }
    
    
    @IBAction func buttonCloseTouched(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
