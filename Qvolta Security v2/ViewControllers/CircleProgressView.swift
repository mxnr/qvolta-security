//
//  CircleProgressView.swift
//  Qvolta Security v2
//
//  Created by Andrii Gusarov on 1/20/18.
//  Copyright © 2018 Evgeniy Sychov. All rights reserved.
//

import UIKit

@IBDesignable class CircleProgressView: UIView {
    
    // MARK: - Properties
    
    // MARK: Last Seconds View
    
    var backgroundImageView: UIImageView! = UIImageView()
    var lastSecondsView: UIView!
    var lastSecondsLabel: UILabel!
    
    private var isTimerVisible: Bool = false
    
    // MARK: Design
    
    struct Normals {
        static let Width: CGFloat = 325.0
        static let Margin: CGFloat = 22.0
        static let Stroke: CGFloat = 12.0
    }
    
    private var currentScale: CGFloat = 1.0
    
    // MARK: System
    
    private var seconds: CFTimeInterval = 0
    private var circleLayer: CAShapeLayer!
    private var timer: Timer?
    
    // MARK: Configuration
    
    var durationOfCircle: Float = 60
    var durationOfWarning: Float = 15
    
    var startColor: UIColor = UIColor(red: 16.0 / 255.0, green: 217.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0)
    var endColor: UIColor = UIColor(red: 223.0 / 255.0, green: 51.0 / 255.0, blue: 50.0 / 255.0, alpha: 1.0)
    
    // MARK: - Methods
    // MARK: Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
        backgroundImageView.image = #imageLiteral(resourceName: "CodeCircleBackground")
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        let theBundle = Bundle(for: self.classForCoder)
        backgroundImageView.image = UIImage.init(named: "CodeCircleBackground", in: theBundle, compatibleWith: nil)
        
        layoutIfNeeded()
    }
    
    // MARK: - Setup

    func setup() {
        setupBackgroundView()
        setupLayer()
        setupLastSeconds()
    }
    
    func setupBackgroundView() {
        addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.contentMode = .scaleAspectFit
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[imageView]|",  metrics: nil, views: ["imageView" : backgroundImageView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[imageView]|",  metrics: nil, views: ["imageView" : backgroundImageView]))
    }
    
    func setupLastSeconds() {
        lastSecondsView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        lastSecondsView.layer.cornerRadius = 25
        lastSecondsView.backgroundColor = .clear//endColor
        
        lastSecondsView.layer.backgroundColor = startColor.cgColor
        
        lastSecondsLabel = UILabel(frame: lastSecondsView.bounds)
        lastSecondsLabel.font = UIFont.init(name: "SFProDisplay-Regular", size: 20)
        lastSecondsLabel.textColor = .white
        lastSecondsLabel.textAlignment = .center
        lastSecondsLabel.backgroundColor = .clear
        
        lastSecondsView.addSubview(lastSecondsLabel)
        
        lastSecondsView.alpha = 0.0
        
        addSubview(lastSecondsView)
    }

    func setupLayer() {
        circleLayer = CAShapeLayer()
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor =  UIColor(red: 16.0 / 255.0, green: 217.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0).cgColor
        circleLayer.lineWidth = Normals.Stroke * currentScale
        circleLayer.lineCap = kCALineCapRound
        layer.addSublayer(circleLayer)
        
        circleLayer.strokeEnd = 0
    }
    
    // MARK: - UIView methods
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        currentScale = bounds.width / Normals.Width
        
        let path: UIBezierPath = UIBezierPath(arcCenter: CGPoint(x: bounds.midX,
                                                                 y: bounds.midY),
                                              radius: (bounds.width - (Normals.Margin * currentScale)) / 2,
                                              startAngle: CGFloat(-Double.pi / 2),
                                              endAngle: CGFloat(Double.pi + Double.pi / 2),
                                              clockwise: true)

        circleLayer.path = path.cgPath
    }
    
    // MARK: - Animation
    
    func stop() {
        timer?.invalidate()
    }
    
    func animate(from: Float = 0.0) {
        let duration: CFTimeInterval = CFTimeInterval(durationOfCircle - (durationOfCircle * from))
        
        let strokeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        strokeAnimation.fromValue = from
        strokeAnimation.toValue = 1.0
        strokeAnimation.duration = duration
        
        var fromValue: UIColor = startColor
        let toValue: UIColor = endColor
        
        if from > 0 {
            fromValue = fromValue.toColor(toValue, percentage: CGFloat(from * 100))
        }
        
        let colorAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeColor")
        colorAnimation.fromValue =  fromValue.cgColor
        colorAnimation.toValue = toValue.cgColor
        colorAnimation.duration = duration
        
        
        let animateGroup: CAAnimationGroup = CAAnimationGroup()
        animateGroup.animations = [strokeAnimation, colorAnimation]
        animateGroup.duration = duration
        animateGroup.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animateGroup.fillMode = kCAFillModeForwards
        animateGroup.isRemovedOnCompletion = false
        circleLayer.add(animateGroup, forKey: "circleAnimation")
        
        seconds = CFTimeInterval(durationOfCircle * from)
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(withTimeInterval: 1,
                                     repeats: true,
                                     block:
            { [weak self] (timer) in
                guard let `self` = self else {
                    timer.invalidate()
                    
                    return
                }
                
                // If < 45 seconds, just increment and hide Seconds View if present
                guard self.seconds >= CFTimeInterval(self.durationOfCircle - self.durationOfWarning) else {
                    self.seconds = self.seconds + 1
                    
                    self.hideTimerCircle()
                    return
                }
                
                // If it is 45th second - show from 3/4 of screen
                if self.seconds == CFTimeInterval(self.durationOfCircle - self.durationOfWarning) {
                    self.playTimerCircle()
                }
                
                // If not - calculate start point
                if self.isTimerVisible == false {
                    self.playTimerCircle(from: Int(self.seconds))
                }
                
                // Update Seconds label
                self.lastSecondsLabel.text = "\(Int(self.durationOfCircle) - Int(self.seconds))"
                
                // End on 59-th second
                // Because there will be new cycle on 60-th
                if self.seconds == CFTimeInterval(self.durationOfCircle) - 1 {
                    self.playEndTimerCircle()
                    self.timer = nil
                    timer.invalidate()
                }
                
                self.seconds = self.seconds + 1
        })
        
        timer?.fire()
    }
    
    // MARK: Last seconds timer
    
    func hideTimerCircle () {
        guard self.isTimerVisible else { return }
        
        lastSecondsView.layer.removeAllAnimations()
        lastSecondsView.alpha = 0
    }
     
    func playTimerCircle(from: Int = 45) {
        lastSecondsView.layer.removeAllAnimations()
        self.isTimerVisible = true
        
        lastSecondsView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration: 0.2) {
            self.lastSecondsView.transform = CGAffineTransform.identity
            self.lastSecondsView.alpha = 1
        }
        
        let percentage: CGFloat = (CGFloat(durationOfCircle) - CGFloat(from)) / CGFloat(durationOfWarning)
        
        let startAngle = (Double.pi / 2) * Double(1.0 - percentage)
        
        let duration = CFTimeInterval(durationOfCircle - Float(from))
        
        let colorAnimation: CABasicAnimation = CABasicAnimation(keyPath: "backgroundColor")
        colorAnimation.fromValue = startColor.toColor(endColor, percentage: CGFloat(from) / CGFloat(durationOfCircle) *  100.0).cgColor
        colorAnimation.toValue = endColor.cgColor
        colorAnimation.duration = duration
        colorAnimation.isRemovedOnCompletion = false
        colorAnimation.fillMode = kCAFillModeForwards
        
        let path: UIBezierPath = UIBezierPath(arcCenter: CGPoint(x: bounds.midX,
                                                                 y: bounds.midY),
                                              radius: (bounds.width - (Normals.Margin * currentScale)) / 2,
                                              startAngle: CGFloat(Double.pi + startAngle),
                                              endAngle: CGFloat(Double.pi + Double.pi / 2),
                                              clockwise: true)
        
        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        pathAnimation.path = path.cgPath
        pathAnimation.duration = duration
        pathAnimation.isRemovedOnCompletion = false
        pathAnimation.fillMode = kCAFillModeForwards
        
        let animateGroup: CAAnimationGroup = CAAnimationGroup()
        animateGroup.animations = [colorAnimation, pathAnimation]
        animateGroup.duration = duration
        animateGroup.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animateGroup.fillMode = kCAFillModeForwards
        animateGroup.isRemovedOnCompletion = false
        
        lastSecondsView.layer.add(animateGroup, forKey: "View animation")
    }
    
    
    func playEndTimerCircle() {
        self.isTimerVisible = false
        
        // Delay to end animation on 60-th second
        // when we start new cycle
        UIView.animate(withDuration: 0.2,
                       delay: 1.0,
                       options: .curveEaseOut,
                       animations: {
                        self.lastSecondsView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                        self.lastSecondsView.alpha = 0
        }, completion: nil)
    }
}
